/**
* @description
* * handles internals for finding a video URL,
* * creates a string to paste into terminal that downloads the video via yt-dlp,
* * attaches this string to clipboard when user presses a button.
*
* @date 2024-05-08
* @class YoutubeDLHelper
*/

/* global GM_setClipboard UserscriptUtils */
// YoutubeDLHelper.js
console.log('/* -------- loading YoutubeDLHelper.js -------- */');
console.groupCollapsed('YoutubeDLHelper');

const { assert } = console;
assert(UserscriptUtils);
// eslint-disable-next-line no-unused-vars
class YoutubeDLHelper {
	static COMMAND_NAME = 'yt-dlp';

	// finds characters not allowed in filenames
	static INVALID_CHARACTER_REGEX = /["%*/:<>?\\|]/gi;

	static MAX_PATH_LENGTH = 260; // win 10 doesn't allow paths > 260 characters

	static MAX_FILE_LENGTH = 75;	// maximum filename length

	static BUTTON_ID = '#userscript-ytdlp-button';

	static BUTTON_TEXT = 'get ytdlp command';

	static COMMAND = (title, url, episode) => {
		const ep = `${(episode) ? ` ${episode}` : ''}`;
		const string = `yt-dlp -o "${title}${ep}.mp4" "${url}"`;
		return string;
	};

	/// /  constructor({
	/// /  	installPath,
	/// /  	titleQuery   = 'h1',
	/// /  	resourceRegex        = /.+.mp4/g,
	/// /  	episodeQuerySelector = '.false',
	/// /  	getTitleFunction     = YoutubeDLHelper.getMovieTitle,
	/// /  	getEpisodeFunction   = YoutubeDLHelper.getEpisodeTitle,
	/// /  	getUrlFunction       = YoutubeDLHelper.getMovieUrl,
	/// /  }) {
	/// /  	this.installPath          = installPath;
	/// /  	this.titleQuery   = titleQuery;
	/// /  	this.resourceRegex        = resourceRegex;
	/// /  	this.episodeQuerySelector = episodeQuerySelector;
	/// /  	this.getTitleFunction     = getTitleFunction;
	/// /  	this.getEpisodeFunction   = getEpisodeFunction;
	/// /  	this.getUrlFunction       = getUrlFunction;
	/// /  }

	/**
  * Creates an instance of YoutubeDLHelper.
  * @date 2024-05-09
  * @param {*} {
  * 		urlMatchRegex       given a url, provides a match if it is the source video
  * 		titleQuery          querySelector query to find the title
  * 		getTitle            <optional> function that returns the title string of the video.
  * 		getVideoURL         <optional> function that returns the url that points to the video.
  * 		getButtonLocation   <optional> function that returns the dom location for the button.
  * 	}
  * @memberof YoutubeDLHelper
  */
	constructor({
		// given a url, provides a match if it is the source video
		// matches strings ending in '.mp4' and '.m3u8'
		urlMatchRegex = /.+((\.m3u8)|(\.mp4))$/g,
		// querySelector query to find the title
		titleQuery = 'h1', // often titles are put in 'h1' headers
		// <optional> function that returns the title string of the video.
		getTitle = YoutubeDLHelper.getTitle,
		// <optional> function that returns the url that points to the video.
		getVideoURL = YoutubeDLHelper.getVideoURL,
		// <optional> function that returns the dom location for the button.
		getButtonLocation = YoutubeDLHelper.getButtonLocation,

		verbose = false,
		debug = false,
	}) {
		if (verbose) {
			console.log('constructor');
			this.verbose = verbose;
		}
		this.urlMatchRegex = urlMatchRegex;
		this.titleQuery = titleQuery;
		this.getTitle = getTitle;
		this.getVideoURL = getVideoURL;
		this.getButtonLocation = getButtonLocation;
		this.debug = debug;

		this.getUrlFromResources = YoutubeDLHelper.getUrlFromResources;

		assert(this.urlMatchRegex, 'urlMatchRegex is falsey');
		assert(this.titleQuery, 'titleQuery is falsey');
		assert(this.getTitle, 'getTitle is falsey');
		assert(this.getVideoURL, 'getVideoURL is falsey');
		assert(this.getButtonLocation, 'getButtonLocation is falsey');
	}

	init() {
		const { waitForElement } = UserscriptUtils;
		waitForElement(this.titleQuery).then(() => {
			if (this.verbose) {
				const ele = document.querySelector(this.titleQuery);
				console.log(ele);
			}
			this.urlString = this.getVideoURL();
			this.title = this.getTitle(this.titleQuery);
			this.button = this.getButton();
			this.buttonLocation = this.getButtonLocation(this.titleQuery);
			if (this.verbose) {
				console.log('this.button :>> ', this.button);
			}
			this.buttonLocation.after(this.button);
		});
	}

	static getVideoURL() {
		const url = YoutubeDLHelper.getUrlFromVideo()
    || YoutubeDLHelper.getUrlFromResources();
		assert(url, 'could not find url');
		return url;
	}

	static getUrlFromVideo() {
		// get an array of all video objects
		const queryResults = document.querySelectorAll('viero');
		const videoArray   = Array.from(queryResults);
		const videoMatches = videoArray.filter((item) => {
			const { src } = item;
			const match   = src?.match(this.urlMatchRegex);
			return match;
		});
		return videoMatches[0] || false;
	}

	static getUrlFromResources() {
		const entries = performance?.getEntries();
		assert(entries?.length, 'could not find resource entries');

		console.groupCollapsed('urlFromResources');
		console.log(entries);
		const sorted = entries.sort((a, b) => b.startTime - a.startTime);
		const matches = sorted.filter((resource) => {
			const { name } = resource;
			console.log(name);
			const match = name?.match(this.urlMatchRegex);
			return match;
		});

		const urls = matches.map((match) => match.name);

		console.groupEnd();
		return urls[0] || false;
	}

	static getTitle(query) {
		return document.querySelector(query).innerText;
	}

	/**
  * creates a button that saves the download string to clipboard
  * @description
  * @date 2024-05-08
  * @memberof YoutubeDLHelper
  */
	getButton() {
		if (this.button) {
			return this.button;
		}
		if (document.querySelector(YoutubeDLHelper.BUTTON_ID)) {
			console.warn('YoutubeDl button already Exists!');
		}
		const button = document.createElement('button');

		button.id = YoutubeDLHelper.BUTTON_ID;
		button.textContent = YoutubeDLHelper.BUTTON_TEXT;

		button.addEventListener('click', () => {
			if (this.debug) {
				// eslint-disable-next-line no-debugger
				debugger;
			}
			this.buttonListener(button);
		});
		return button;
	}

	static getButtonLocation(query) {
		if (this.verbose) {
			console.log('get button location', query);
		}
		return document.querySelector(query);
	}

	addButtonToPage(button = this.button) {
		const titleObject = this.getTitleObject();
		titleObject.after(button);
	}

	getMovieUrl(
		urlQuery = this.urlQuery,
		resourceRegex = this.resourceRegex,
	) {
		const urlFromQuery     = this.getUrlFromQuery(urlQuery);
		const urlFromResources = this.getUrlFromResources(resourceRegex);

		return urlFromQuery || urlFromResources;
	}

	getUrlFromQuery(urlQuery = this.urlQuery) {
		if (!urlQuery) {
			return false;
		}

		const queryResult = document.querySelector(urlQuery);
		const url = queryResult.src;

		return url;
	}

	getMovieTitle(titleQuery = this.titleQuery) {
		return this.getFileNameFromQuery(titleQuery);
	}

	getEpisodeTitle(episodeQuerySelector = this.episodeQuerySelector) {
		if (!episodeQuerySelector) {
			return '';
		}
		const object = document.querySelector(episodeQuerySelector);
		const string = object?.textContent?.trim() || '';
		return string;
	}

	/* eslint-disable no-param-reassign */
	buttonListener(button) {
		console.log(this);
		let movieUrl = this.getMovieUrl();
		const movieTitle = this.getMovieTitle();
		const episodeTitle = this.getEpisodeTitle();
		let output = this.getCommandLine(movieTitle, movieUrl, episodeTitle);

		if (movieUrl && movieTitle && output) {
			movieUrl = this.getMovieTitle();
			output = this.getCommandLine(movieTitle, movieUrl, episodeTitle);
			GM_setClipboard(output);
			button.style = 'background: green;';
			return true;
		}

		button.style = 'background: red;';
		return false;
	}
	/* eslint-enable no-param-reassign */

	getTitleObject(titleQuery = this.titleQuery) {
		const titleObject = document.querySelector(titleQuery);
		assert(titleObject, 'no titleobject', titleQuery);
		return titleObject;
	}

	getFileNameFromQuery(titleQuery = this.titleQuery) {
		const titleObject = this.getTitleObject(titleQuery);
		const rawText = titleObject.textContent;

		const fileName = this.getFileNameFromString(rawText);

		this.fileName = fileName;
		return fileName;
	}

	getFileNameFromString(string) {
		const regex = YoutubeDLHelper.INVALID_CHARACTER_REGEX;
		const validFileName = string.replaceAll(regex, '');
		const trimmedFileName = validFileName.trim();
		const fileName = trimmedFileName.slice(0, YoutubeDLHelper.MAX_FILE_LENGTH);

		this.fileName = fileName;
		return fileName;
	}

	getCommandLine(
		title = this.fileName,
		url = this.url,
		episode = this.episode,
	) {
		console.log(title, url);
		if (!title || !url) {
			return false;
		}
		const command = YoutubeDLHelper.COMMAND(title, url, episode);
		console.log(command);
		return command;
	}
}
console.groupEnd();
console.log('/* ------ YoutubeDLHelper.js done loading ----- */');
