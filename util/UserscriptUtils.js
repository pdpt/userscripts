// UserscriptUtils.js
console.log('/* -------- loading UserscriptUtils.js -------- */');
console.group('UserscriptUtils');
// eslint-disable-next-line no-unused-vars
const UserscriptUtils = {
	waitForElement(selector) {
		return new Promise((resolve) => {
			if (document.querySelector(selector)) {
				resolve(document.querySelector(selector));
			}

			const observer = new MutationObserver(() => {
				if (document.querySelector(selector)) {
					resolve(document.querySelector(selector));
					observer.disconnect();
				}
			});

			observer.observe(document.body, {
				subtree:   true,
				childList: true,
			});
		});
	},
};
console.groupEnd();
console.log('/* ------ UserscriptUtils.js done loading ----- */');
