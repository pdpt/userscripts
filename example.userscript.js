// see: <https://violentmonkey.github.io/api/metadata-block/
// ==UserScript==
//
// @name				${1:userscriptname} - ${2:hostname}
// @namespace	<gitlabURL>
// @author			pdpt
// @version		0.0.1
//
// @downloadURL	<gitlabURL>/${1:userscriptname}/releases/lateest
// @supportURL		<gitlabURL>/${1:userscriptname}
// @homepageURL	<gitlabURL>
//
// @icon						${2:hostname}/favicon.ico
// @match					${2:hostname}/${3:*}
// @exclude-match
//
// @run-at				document-end
// //@inject-into	//TODO: learn about this
// //@noframes
//
// @grant	none
//
// //@require	<>
// //@resource	CSS_RESOURCE <>
//
// @description	${4:short description}
//
// * optional requires below.
//
// //@require	<>
// ==/UserScript==
(() => {
	console.log('Ok');
})();
