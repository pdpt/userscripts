// ==UserScript==
// @name        add copy release ID button - musicbrainz.org
// @namespace   Violentmonkey Scripts
// @match       https://musicbrainz.org/*
// @grant       none
// @version     1.0
// @author      pdpt
// @description add a button next to track titles, that copies the track ID to clipboard. for use with beets.io
// @grant       GM_setClipboard
// @run-at      document-idle
// ==/UserScript==
/* global GM_setClipboard */
/**
 * (?<=y)x look behind : Matches "x" only if "x" is preceded by "y".
 * .                   : Matches any single character except line terminators
 * x*                  : Matches the preceding item "x" 0 or more times.
 *
 * select all characters AFTER finding "\recording\"
 *
 * "https://musicbrainz.org/recording/0950714a-dff1-4395-b849-20638cf2cfde"
 * gives
 * 0950714a-dff1-4395-b849-20638cf2cfde
 */

const createCopyButton = function createCopyButton(ID) {
	const button = document.createElement('button');
	const style  = 'font-size: 7.2px;margin-right: 10px;';

	button.textContent = 'Copy ID';
	button.style       = style;

	button.addEventListener('click', () => {
		GM_setClipboard(ID); // eslint-disable-line new-cap
		button.textContent = 'Copied!';
		button.style = `${style}background: green;`;
	});

	return button;
};

(() => {
	const keywords = ['/recording/', '/release/'];

	keywords.forEach((keyword) => {
		const queryResults = document.querySelectorAll(`a[href*='${keyword}']`);
		const queryArray = [...queryResults]
			// remove results like `/${key}/${ID}/tags`
			.filter((element) => (element.pathname.split('/').length === 3));

		queryArray.forEach((element) => {
			const { pathname } = element;
			const id = pathname.replace(keyword, ''); // get the musicbrainz ID
			const button = createCopyButton(id);      // create a new button that puts the ID in clipboard
			element.before(button); // insert button before the existing link
		});
	});
})();
