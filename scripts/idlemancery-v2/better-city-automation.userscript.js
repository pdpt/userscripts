// see: <https://violentmonkey.github.io/api/metadata-block/
// ==UserScript==
//
// @name				better city automation - idlemancery-v2.vercel.app
// @namespace	<gitlabURL>
// @author			pdpt
// @version		0.0.1
//
// @downloadURL	<gitlabURL>/${1:userscriptname}/releases/lateest
// @supportURL		<gitlabURL>/${1:userscriptname}
// @homepageURL	<gitlabURL>
//
// @icon						https://idlemancery-v2.vercel.app/theme/img/runes/willpower-v2.png
// @match					http*://idlemancery-v2.vercel.app/
// @exclude-match
//
// @run-at				document-end
// //@inject-into	//TODO: learn about this
// //@noframes
//
// @grant	none
//
// //@require	<>
// //@resource	CSS_RESOURCE <>
//
// @description	WIP
//
// *************************************************************************************************
// *            YOU MAY REMOVE ANY OF THE @require LINES BELOW FOR SCRIPTS YOU DO NOT WANT         *
// *************************************************************************************************
//
// //@require	<>
// ==/UserScript==
/* global UserscriptUtils */

/*
 * int wp rune str vit agi spell conj stam

			name 											| improves		|	cost								|	impacted
-----------------------------------------------------------------------------------
00		Rest											| energy			|											|	vit
01		Push Up										| str					|	energy							| vit75, str25
02		Breath Techniques					| vit					| energy							| vit50, str50
03		Investigate World					| int					| energy							| vit75, str25
04		Train Endurance						| vit 				| energy, money				| vit75, str25
05		Train Strength						| str 				| energy, money				| vit75, str25
06		Read Books								| knw cap 		| energy, mana				| int75, wp25
07		Read Manuscripts					| rune				| energy, mana				| int75, wp25
08		Mental Aptitude						| int					| energy, knw 				|	vit25, wp75
09		Brainstorm								| knw					| energy, mana				| wp50, int50
10		Meditate									| mana cap		|	eneryg, knw					| int75, vit25
11		Willpower Training				| wp					| energy, knw					| int
12		Rune Lessons							| Rune				| energy, knw					| int75, wp25
13		Condense Stashes					| money cap		|	energy, mana				| int5, spell95
14		Heart Spark Infusion			| vit					| energy, spark, knw	| spell
15		Muscle Spark Infusion			| str					| energy, spark, knw	| spell
16		Brain Spark Infusion			| int					| energy, spark, knw	| spell
17		Magic Lessons							| spell				| energy, spark, knw	| int75, wp25
18		Improve Spark Storage			| spark cap		| energy, knw					| int75, wp25
19		Conjuration Training			| conj				| energy, mana				| vit75, wp25
20		Enchant Fingers						| agi					| energy, mana				| spell

stat 	| improved by | improves
int 	| 03, 08, 16	|	06, 07, 10, 11, 12, 17, 18
wp		| 11. 				| 06, 07, 08, 09, 12, 17, 18, 19,
rune	|	07, 12,			|
str		| 01, 05, 15, | 01, 02, 03, 04, 05,
vit		| 02, 04, 14, | 00, 01, 02, 03, 04, 05, 08, 10, 19
agi		| 20					|
spell	|	17, 				| 13, 14, 15, 16, 20,
conj	| 19, 				|
stam	|							|

money            | str
craft            | str
battle trainingg | str
fight            | stam
conj 	           | spell99, int1

Weapon Mastery Training		|
Battle Endurance Training	|
Fight											|
Craft											|
Clean Streets						| money 		| energy 							| str
Dig Channels	 					| money 		| energy 							| str
Carry Bags	   					| money 		| energy 							| str
Labour	       					| money 		| energy 							| str
Farmer	       					| money 		| energy 							| str
Conjure Spark						| spark			| energy, mana				| spell99, int1
Conjure Wood						| spark			| energy, mana				| spell99, int1
Conjure Stone						| spark			| energy, mana				| spell99, int1
Conjure Iron						| spark			| energy, mana				| spell99, int1
*/

/** *********************************************************************** */

// up click   				clientX: 1416, clientY: 127,
// down click 				clientX: 1417, clientY: 137,

// document.querySelector('.heading .filter-flex-wrap input').getBoundingClientRect()
/**
 	*  document.querySelector('.heading .filter-flex-wrap input').getBoundingClientRect() => {
	* 	bottom: 150.5
	*		height: 35
	*		left: 1238.86669921875
	*		right: 1435.86669921875
	*		top: 115.5
	*		width: 197
	*		x: 1238.86669921875
	*		y: 115.5
	* }
 	*/
/**
 * so, up click is (right-20, top+12)
 *   down click is (right-20, top+22)
 */

// eslint-disable-next-line func-names
(function () {
	/** Code Here * */

	const ACTION_INTERVAL = 1000; // * 10;
	const filterQuery = '.city-wrap .heading .filter-flex-wrap input';

	let toggleIntervalButton;
	let filterInput = document.querySelector(filterQuery);
	let myInterval;

	const startRandomAction = function startRandomAction() {
		// document.querySelector(".action-list .ui-icon").click()
		const actionList = document.querySelectorAll('.action-list .ui-icon');
		const children = Array.from(actionList);
		const randChild = children[Math.floor(Math.random() * children.length)];
		randChild.click();
	};

	const startInterval = function startInterval() {
		console.log('startInterval');
		// setPercentIncr(100);
		if (!myInterval) {
			myInterval = setInterval(() => {
				// console.log("interval100");
				startRandomAction();
			}, ACTION_INTERVAL);
		}
	};

	const stopInterval = function stopInterval() {
		if (myInterval) {
			clearInterval(myInterval);
			myInterval = false;
		}
	};

	const addButtons = function createButtons() {
		filterInput.after(toggleIntervalButton);
	};

	const isCity = function isCity() {
		filterInput = document.querySelector(filterQuery);
		return !!filterInput;
	};

	const toggleButton = function toggleButton() {
		console.log('click');
		if (myInterval) {
			stopInterval();
			toggleIntervalButton.innerText = 'OFF';
		} else {
			startInterval();
			toggleIntervalButton.innerText = 'ON';
		}
	};

	/**
   * todo
   * add a checkbox for each action
   * the checkbox determines if the action should be selected or not
   * we need to keep track of checkbox status because it doesn't persist between tab switching.
   */
	const addCheckBoxes = function addCheckBoxes() {
		const actionList = document.querySelectorAll('.action-list .ui-icon');
		const children = Array.from(actionList);
		children.forEach((child) => {
			const parent = child.parentElement;
			if (!parent.querySelector('.userScript_checkbox')) {
				const checkBox = document.createElement('input');
				checkBox.type = 'checkBox';
				checkBox.classList.add('userScript_checkbox');
				child.after(checkBox);
			}
		});
	};

	const createDomElements = function createDomElements() {
		toggleIntervalButton = document.createElement('button');
		toggleIntervalButton.innerText = 'OFF';
		toggleIntervalButton.addEventListener('click', toggleButton, false);
	};

	let cityInterval;

	/**
   * starts the interval to check if we are on the city tab.
   */
	const startCityInterval = function startCityInterval() {
		if (cityInterval) return;
		cityInterval = setInterval(() => {
			const flag = isCity();
			if (flag) {
				addButtons();
				addCheckBoxes();
			}
		}, 100);
	};

	const init = function init() {
		const { waitForElement } = UserscriptUtils;
		// create buttons and such to be added when page loads
		createDomElements();

		waitForElement('.page-inner').then(() => {
			startCityInterval();
		});

		const waitInterval = setInterval(() => {
			// if page loaded
			if (document.querySelector('.page-inner')) {
				// start interval to check if city
				setInterval(() => {
					const flag = isCity();
					// console.log('flag :>> ', flag);
					if (flag) {
						addButtons();
						addCheckBoxes();
					} else {
						// removeButtons();
					}
				}, 100);
				clearInterval(waitInterval);
			}
		}, 300);
	};

	init();
}());
