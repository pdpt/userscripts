/**
 * @header Copyright 2024 Taylor Premo <tpre@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	eslint.config.js
 */
import globals from 'globals';

import mainConfig from '../eslint.config.js';
// import globals from "globals";

// eslint.config.js
export default [
	...mainConfig,
	{
		files:           ['**/*.js'],
		languageOptions: {
			globals: {
				...globals.browser,
			},
		},
		rules: {
		},
	},
];
